import React, { useState, useMemo } from 'react';
// External
import { useForm } from 'react-hook-form';
import { useLoaderData, useNavigate, useNavigation } from 'react-router-dom';
import { Card } from '@material-tailwind/react';
import { Listbox, Combobox } from '@headlessui/react';
import { toast } from 'react-toastify';
import LoadingOverlay from 'react-loading-overlay-ts';
import axios from 'axios';
// Icons
import {
  PencilSquareIcon,
  ArrowUturnLeftIcon,
  XMarkIcon,
  CheckCircleIcon,
  CubeIcon,
  ChevronUpDownIcon,
} from '@heroicons/react/24/solid';
// utils
import { Auth } from 'utils/auth';
import { apiUrl } from 'utils/constants';
import { provinces } from 'utils/locations';
import { renderStatus } from 'utils/helper';

export default function UserView() {
  const { user } = (useLoaderData() as { user }) || {
    user: { data: {} },
  };
  const [loading, setLoading] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);

  const statusOptions = [1, 0];
  const userTypeOptions = [
    'admin_user',
    'logistics_user',
    'accounting_user',
    'product_user',
  ];

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: useMemo(() => {
      return user;
    }, [user]),
  });

  type BasicModel = {
    id: number;
    name: string;
  };

  const navigate = useNavigate();
  const navigation = useNavigation();

  const [status, setStatus] = useState(user.status);
  const [userType, setUserType] = useState(user.user_type);

  const prvnc = provinces.filter(province => {
    return (
      province.name.toLowerCase() === user.province &&
      user.province.toLowerCase()
    );
  });
  const [selectedLocation, setSelectedLocation] = useState(prvnc[0]);

  const onSubmit = async data => {
    try {
      data.status = status;
      data.user_type = userType;
      data.province = selectedLocation.name;
      setLoading(true);
      const response = await axios.put(`${apiUrl}/v2/account/update`, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Auth.token()}`,
        },
      });
      if (response.status === 200) {
        setLoading(false);
        setIsEditMode(false);
        toast.success('User Updated!', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'colored',
        });
        navigate(`/users/${data.id}`, { replace: true });
      } else {
        setLoading(false);
        setIsEditMode(false);

        toast.error('User update failed!', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'colored',
        });
      }
    } catch (error) {
      setLoading(false);
      setIsEditMode(false);
      toast.error('Something unexpected happened!', {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };
  const [provinceQuery, setProvinceQuery] = useState('');

  const handleSelectedProvince = p => {
    const filterProvince =
      p === ''
        ? provinces
        : provinces.filter(province => {
            return province.name.toLowerCase().includes(p.toLowerCase());
          });
    setSelectedLocation(filterProvince[0]);
  };

  const filteredProvince =
    provinceQuery === ''
      ? provinces
      : provinces.filter(province => {
          return province.name
            .toLowerCase()
            .includes(provinceQuery.toLowerCase());
        });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <LoadingOverlay
        active={navigation.state === 'loading' || loading === true}
        className="m-4 p-4 flex flex-col md:flex-row gap-2"
      >
        <div className="w-full md:w-2/6 md:order-1 grow-0 space-y-2">
          <Card className="hover:shadow-md p-4">
            {isEditMode ? (
              <div className="my-2 flex items-center justify-between">
                <button
                  onClick={e => {
                    e.preventDefault();
                    setIsEditMode(false);
                  }}
                  className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
                >
                  <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-red-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                  <span className="absolute rounded-lg inset-0 w-full h-full bg-red-600 border-2 border-red-600 group-hover:bg-primary-500 group-hover:border-red-600"></span>
                  <span className="relative text-white group-hover:text-red-600 flex items-center justify-center text-xs">
                    <XMarkIcon className="h-4 w-4 mr-2" />
                    Cancel
                  </span>
                </button>
                <button
                  type="submit"
                  className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
                >
                  <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-primary-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                  <span className="absolute rounded-lg inset-0 w-full h-full bg-primary-500 border-2 border-primary-500 group-hover:bg-secondary-500 group-hover:border-primary-500"></span>
                  <span className="relative text-secondary-500 group-hover:text-primary-500 flex items-center justify-center text-xs">
                    <CheckCircleIcon className="h-4 w-4 mr-2" />
                    Save
                  </span>
                </button>
              </div>
            ) : (
              <div className="my-2 flex items-center justify-between">
                <button
                  onClick={e => {
                    e.preventDefault();
                    navigate(`/users`);
                  }}
                  className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
                >
                  <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-secondary-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                  <span className="absolute rounded-lg inset-0 w-full h-full bg-secondary-500 border-2 border-secondary-500 group-hover:bg-primary-500 group-hover:border-secondary-500"></span>
                  <span className="relative text-primary-500 group-hover:text-secondary-500 flex items-center justify-center text-xs">
                    <ArrowUturnLeftIcon className="h-4 w-4 mr-2" />
                    Back
                  </span>
                </button>
                <button
                  onClick={e => {
                    e.preventDefault();
                    setIsEditMode(true);
                  }}
                  className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
                >
                  <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-primary-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                  <span className="absolute rounded-lg inset-0 w-full h-full bg-primary-500 border-2 border-primary-500 group-hover:bg-secondary-500 group-hover:border-primary-500"></span>
                  <span className="relative text-secondary-500 group-hover:text-primary-500 flex items-center justify-center text-xs">
                    <PencilSquareIcon className="h-4 w-4 mr-2" />
                    Edit
                  </span>
                </button>
              </div>
            )}
          </Card>
          {user.vendor && (
            <Card>
              <div className="my-2 p-4 flex items-center justify-between text-sm">
                <p className="text-primary-200">Vendor</p>
                <b className="text-primary-500 flex items-center">
                  <CubeIcon className="h-4 w-4 ml-2 mr-1 text-secondary-500" />
                  {user.vendor.name}
                </b>
              </div>
            </Card>
          )}
          {user.garage && (
            <Card>
              <div className="my-2 p-4 flex items-center justify-between text-sm">
                <p className="text-primary-200">Garage</p>
                <b className="text-primary-500 flex items-center">
                  <CubeIcon className="h-4 w-4 ml-2 mr-1 text-secondary-500" />
                  {user.garage.shop_name}
                </b>
              </div>
            </Card>
          )}
        </div>
        <Card className="flex-1 max-w-full p-4 grid grid-cols-4 gap-2 text-xs hover:shadow-md">
          <div className="col-span-4 text-left my-4 relative">
            <p className="text-secondary-500 text-md text-lg font-semibold">
              {isEditMode ? 'Edit User' : ' User Details'}
            </p>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              First Name
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.first_name
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="text"
              disabled={!isEditMode}
              name="first_name"
              type="text"
              {...register('first_name', { required: true })}
            />
            {errors.first_name && (
              <p className="text-red-500 text-xs italic">
                First Name is required
              </p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Last name
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.last_name
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="text"
              name="last_name"
              type="text"
              disabled={!isEditMode}
              {...register('last_name', { required: true })}
            />
            {errors.last_name && (
              <p className="text-red-500 text-xs italic">
                Last name is required
              </p>
            )}
          </div>
          <div className="col-span-4 mb-2">
            <label className="block text-slate-700 font-bold mb-2">Email</label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.email
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="text"
              name="email"
              type="text"
              disabled={!isEditMode}
              {...register('email', { required: true })}
            />
            {errors.email && (
              <p className="text-red-500 text-xs italic">Email is required</p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              Status
            </label>
            <Listbox value={status} onChange={setStatus} disabled={!isEditMode}>
              <Listbox.Button
                className={`text-left max-w-sm appearance-none border rounded-md  w-full py-2 px-3  leading-tight focus:outline-none focus-visible:border-secondary-500 ${
                  isEditMode
                    ? 'border-slate-200 bg-slate-50 text-slate-700'
                    : 'border-gray-300 bg-gray-200 text-slate-700'
                }`}
              >
                {renderStatus(status)}
              </Listbox.Button>
              <Listbox.Options className="absolute mt-1 z-40 max-h-60 w-full overflow-auto rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                {statusOptions.map(stat => (
                  <Listbox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none py-2 pl-5 pr-4 ${
                        active
                          ? 'bg-primary-500 text-secondary-500'
                          : 'text-gray-900'
                      }`
                    }
                    key={stat}
                    value={stat}
                  >
                    {renderStatus(stat)}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Listbox>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              User Type
            </label>
            <Listbox
              value={userType}
              onChange={setUserType}
              disabled={!isEditMode}
            >
              <Listbox.Button
                className={`text-left max-w-sm appearance-none border rounded-md  w-full py-2 px-3  leading-tight focus:outline-none focus-visible:border-secondary-500 ${
                  isEditMode
                    ? 'border-slate-200 bg-slate-50 text-slate-700'
                    : 'border-gray-300 bg-gray-200 text-slate-700'
                }`}
              >
                {userType}
              </Listbox.Button>
              <Listbox.Options className="absolute mt-1 z-40 max-h-60 w-full overflow-auto rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                {userTypeOptions.map(userT => (
                  <Listbox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none py-2 pl-5 pr-4 ${
                        active
                          ? 'bg-primary-500 text-secondary-500'
                          : 'text-gray-900'
                      }`
                    }
                    key={userT}
                    value={userT}
                  >
                    {userT}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Listbox>
          </div>
          {user.user_type === 'garage_user' && (
            <div className="col-span-4 md:col-span-2 mb-2">
              <label className="block text-slate-700 font-bold mb-2">
                Garage ID
              </label>
              <input
                className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                  errors.garage_id
                    ? 'border border-red-500 focus:outline focus:outline-red-500'
                    : 'focus:outline focus:outline-secondary-500'
                } ${
                  isEditMode
                    ? 'border-slate-200 bg-slate-50 text-slate-700'
                    : 'border-gray-300 bg-gray-200 text-slate-700'
                }`}
                id="text"
                name="garage_id"
                type="text"
                disabled={!isEditMode}
                {...register('garage_id', { required: false })}
              />
              {errors.garage_id && (
                <p className="text-red-500 text-xs italic">
                  Garage ID is required
                </p>
              )}
            </div>
          )}
          {user.user_type === 'vendor_user' && (
            <div className="col-span-4 md:col-span-2 mb-2">
              <label className="block text-slate-700 font-bold mb-2">
                Vendor ID
              </label>
              <input
                className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                  errors.vendor_id
                    ? 'border border-red-500 focus:outline focus:outline-red-500'
                    : 'focus:outline focus:outline-secondary-500'
                } ${
                  isEditMode
                    ? 'border-slate-200 bg-slate-50 text-slate-700'
                    : 'border-gray-300 bg-gray-200 text-slate-700'
                }`}
                id="text"
                name="vendor_id"
                type="text"
                disabled={!isEditMode}
                {...register('vendor_id', { required: false })}
              />
              {errors.vendor_id && (
                <p className="text-red-500 text-xs italic">
                  Vendor ID is required
                </p>
              )}
            </div>
          )}
          <div className="col-span-4 text-left my-4 relative">
            <p className="text-secondary-500 text-md text-lg font-semibold">
              Address
            </p>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              Province
            </label>
            <Combobox
              value={selectedLocation}
              onChange={c => handleSelectedProvince(c)}
              as="div"
              disabled={!isEditMode}
              className="mx-auto max-w-xl divide-y divide-gray-100 overflow-hidden"
            >
              <Combobox.Input
                className="`max-w-sm appearance-none border rounded-md border-slate-200 bg-slate-50 w-full py-2 px-3 text-slate-700 leading-tight focus:outline-none focus-visible:border-secondary-500 disabled:border-gray-300 disabled:bg-gray-200"
                onChange={event => setProvinceQuery(event.target.value)}
                displayValue={(province: BasicModel) => province.name}
              />
              <Combobox.Button className="absolute inset-y-0 right-0 mt-6 flex border-none items-center pr-2">
                <ChevronUpDownIcon
                  className="h-4 w-4 text-gray-400"
                  aria-hidden="true"
                />
              </Combobox.Button>
              <Combobox.Options className="absolute bg-white shadow-md z-40 mt-1 max-h-32 md:max-h-64 w-full overflow-y-auto rounded ">
                {filteredProvince.map(province => (
                  <Combobox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none text-xs py-2 pl-5 pr-4 ${
                        active ? 'bg-primary-500 text-white' : 'text-gray-900'
                      }`
                    }
                    key={province.name}
                    value={province.name}
                  >
                    {province.name}
                  </Combobox.Option>
                ))}
              </Combobox.Options>
            </Combobox>
            {errors.name && (
              <p className="text-red-500 text-xs italic">
                Province is required
              </p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">City</label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.city
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="city"
              name="city"
              type="text"
              {...register('city', { required: false })}
            />
            {errors.city && (
              <p className="text-red-500 text-xs italic">City is required</p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Barangay
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.barangay
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="barangay"
              name="barangay"
              type="text"
              {...register('barangay', { required: false })}
            />
            {errors.barangay && (
              <p className="text-red-500 text-xs italic">
                Barangay is required
              </p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Postal
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.postal
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="postal"
              name="postal"
              type="number"
              {...register('postal', { required: false })}
            />
            {errors.postal && (
              <p className="text-red-500 text-xs italic">Postal is required</p>
            )}
          </div>
          <div className="col-span-4 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Address
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight ${
                errors.address
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } ${
                isEditMode
                  ? 'border-slate-200 bg-slate-50 text-slate-700'
                  : 'border-gray-300 bg-gray-200 text-slate-700'
              }`}
              id="addres"
              name="address"
              type="text"
              {...register('address', { required: false })}
            />
            {errors.address && (
              <p className="text-red-500 text-xs italic">Address is required</p>
            )}
          </div>
        </Card>
      </LoadingOverlay>
    </form>
  );
}
