import React, { useState } from 'react';
// External
import { useForm } from 'react-hook-form';
import { useNavigate, useNavigation } from 'react-router-dom';
import { Card } from '@material-tailwind/react';
import { Listbox, Combobox } from '@headlessui/react';
import { toast } from 'react-toastify';
import LoadingOverlay from 'react-loading-overlay-ts';
import axios from 'axios';
// Icons
import {
  CheckCircleIcon,
  ArrowUturnLeftIcon,
  ChevronUpDownIcon,
} from '@heroicons/react/24/solid';
// utils
import { Auth } from 'utils/auth';
import { apiUrl } from 'utils/constants';
import { provinces } from 'utils/locations';
import { renderStatus } from 'utils/helper';

export default function UserView() {
  const [loading, setLoading] = useState(false);

  const statusOptions = [1, 0];
  const userTypeOptions = [
    'admin_user',
    'logistics_user',
    'accounting_user',
    'product_user',
  ];
  interface InputTypes {
    email: string;
    password: string;
    password_confirmation: string;
    firstname: string;
    lastname: string;
    user_type: string;
    address: string;
    postal: string;
    province: string;
    barangay: string;
    city: string;
  }
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<InputTypes>();

  type BasicModel = {
    id: number;
    name: string;
  };

  const onSubmit = async data => {
    try {
      data.status = status;
      data.user_type = userType;
      data.province = selectedLocation.name;
      setLoading(true);
      const response = await axios.post(`${apiUrl}/v2/account/register`, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Auth.token()}`,
        },
      });
      if (response.status === 200) {
        setLoading(false);
        toast.success('User Created!', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'colored',
        });
        navigate(`/users/${response.data.data.user.id}`, {
          replace: true,
        });
      } else {
        setLoading(false);
        toast.error('User failed to create!', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'colored',
        });
      }
    } catch (error) {
      setLoading(false);

      toast.error('Something unexpected happened!', {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  const navigate = useNavigate();
  const navigation = useNavigation();

  const [status, setStatus] = useState(statusOptions[0]);
  const [userType, setUserType] = useState(userTypeOptions[0]);

  const [selectedLocation, setSelectedLocation] = useState(provinces[0]);

  const [provinceQuery, setProvinceQuery] = useState('');

  const handleSelectedProvince = p => {
    const filterProvince =
      p === ''
        ? provinces
        : provinces.filter(province => {
            return province.name.toLowerCase().includes(p.toLowerCase());
          });
    setSelectedLocation(filterProvince[0]);
  };

  const filteredProvince =
    provinceQuery === ''
      ? provinces
      : provinces.filter(province => {
          return province.name
            .toLowerCase()
            .includes(provinceQuery.toLowerCase());
        });

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <LoadingOverlay
        active={navigation.state === 'loading' || loading === true}
        className="m-4 p-4 flex flex-col md:flex-row gap-2"
      >
        <div className="w-full md:w-2/6 md:order-1 grow-0 space-y-2">
          <Card className="hover:shadow-md p-4">
            <div className="my-2 flex items-center justify-between">
              <button
                onClick={e => {
                  e.preventDefault();
                  navigate(`/users`);
                }}
                className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
              >
                <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-secondary-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                <span className="absolute rounded-lg inset-0 w-full h-full bg-secondary-500 border-2 border-secondary-500 group-hover:bg-primary-500 group-hover:border-secondary-500"></span>
                <span className="relative text-primary-500 group-hover:text-secondary-500 flex items-center justify-center text-xs">
                  <ArrowUturnLeftIcon className="h-4 w-4 mr-2" />
                  Back
                </span>
              </button>
              <button
                type="submit"
                className="relative inline-block px-4 py-2 font-bold group w-full max-w-[8rem]"
              >
                <span className="absolute rounded-lg inset-0 w-full h-full transition duration-200 ease-out transform translate-x-1 translate-y-1 bg-primary-200 group-hover:-translate-x-0 group-hover:-translate-y-0"></span>
                <span className="absolute rounded-lg inset-0 w-full h-full bg-primary-500 border-2 border-primary-500 group-hover:bg-secondary-500 group-hover:border-primary-500"></span>
                <span className="relative text-secondary-500 group-hover:text-primary-500 flex items-center justify-center text-xs">
                  <CheckCircleIcon className="h-4 w-4 mr-2" />
                  Save
                </span>
              </button>
            </div>
          </Card>
        </div>
        <Card className="flex-1 max-w-full p-4 grid grid-cols-4 gap-2 text-xs hover:shadow-md">
          <div className="col-span-4 text-left my-4 relative">
            <p className="text-secondary-500 text-md text-lg font-semibold">
              User Details
            </p>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              First Name
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.firstname
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } `}
              id="text"
              name="firstname"
              type="text"
              {...register('firstname', { required: true })}
            />
            {errors.firstname && (
              <p className="text-red-500 text-xs italic">
                First Name is required
              </p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Last name
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.lastname
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } `}
              id="text"
              name="lastname"
              type="text"
              {...register('lastname', { required: true })}
            />
            {errors.lastname && (
              <p className="text-red-500 text-xs italic">
                Last name is required
              </p>
            )}
          </div>
          <div className="col-span-4 mb-2">
            <label className="block text-slate-700 font-bold mb-2">Email</label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.email
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } `}
              id="text"
              name="email"
              type="text"
              {...register('email', { required: true })}
            />
            {errors.email && (
              <p className="text-red-500 text-xs italic">Email is required</p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Password
            </label>
            <input
              className={`max-w-sm appearance-none border rounded-md border-slate-200 bg-slate-50 w-full py-2 px-3 text-slate-700 leading-tight ${
                errors.password
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              }`}
              id="text"
              name="password"
              type="password"
              {...register('password', {
                required: 'Password is required (min 6 characters)',
                minLength: 6,
              })}
            />
            {errors.password && (
              <p className="text-red-500 text-xs italic">
                {errors.password && errors.password.type === 'required'
                  ? 'Password is required'
                  : 'Password must be at least 6 characters long'}
              </p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Confirm Password
            </label>
            <input
              className={`max-w-sm appearance-none border rounded-md border-slate-200 bg-slate-50 w-full py-2 px-3 text-slate-700 leading-tight ${
                errors.password_confirmation
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              }`}
              id="text"
              name="password_confirmation"
              type="password"
              {...register('password_confirmation', {
                required: true,
                validate: (val: string) => {
                  if (watch('password') !== val) {
                    return 'Your passwords do no match';
                  }
                },
              })}
            />
            {errors.password_confirmation && (
              <p className="text-red-500 text-xs italic">
                {errors.password_confirmation &&
                errors.password_confirmation.type === 'required'
                  ? 'Password Confirmation is required'
                  : errors.password_confirmation.message}
              </p>
            )}
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              Status
            </label>
            <Listbox value={status} onChange={setStatus}>
              <Listbox.Button
                className={`text-left max-w-sm appearance-none border rounded-md border-slate-200 bg-slate-50 w-full py-2 px-3  leading-tight focus:outline-none focus-visible:border-secondary-500`}
              >
                {renderStatus(status)}
              </Listbox.Button>
              <Listbox.Options className="absolute mt-1 z-40 max-h-60 w-full overflow-auto rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                {statusOptions.map(stat => (
                  <Listbox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none py-2 pl-5 pr-4 ${
                        active
                          ? 'bg-primary-500 text-secondary-500'
                          : 'text-gray-900'
                      }`
                    }
                    key={stat}
                    value={stat}
                  >
                    {renderStatus(stat)}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Listbox>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              User Type
            </label>
            <Listbox value={userType} onChange={setUserType}>
              <Listbox.Button
                className={`text-left max-w-sm appearance-none border-slate-200 bg-slate-50 border rounded-md  w-full py-2 px-3  leading-tight focus:outline-none focus-visible:border-secondary-500 `}
              >
                {userType}
              </Listbox.Button>
              <Listbox.Options className="absolute mt-1 z-40 max-h-60 w-full overflow-auto rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                {userTypeOptions.map(userT => (
                  <Listbox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none py-2 pl-5 pr-4 ${
                        active
                          ? 'bg-primary-500 text-secondary-500'
                          : 'text-gray-900'
                      }`
                    }
                    key={userT}
                    value={userT}
                  >
                    {userT}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Listbox>
          </div>

          <div className="col-span-4 text-left my-4 relative">
            <p className="text-secondary-500 text-md text-lg font-semibold">
              Address
            </p>
          </div>
          <div className="col-span-4 md:col-span-2 mb-2 relative">
            <label className="block text-slate-700 font-bold mb-2">
              Province
            </label>
            <Combobox
              value={selectedLocation}
              onChange={c => handleSelectedProvince(c)}
              as="div"
              className="mx-auto max-w-xl divide-y divide-gray-100 overflow-hidden"
            >
              <Combobox.Input
                className="`max-w-sm appearance-none border rounded-md border-slate-200 bg-slate-50 w-full py-2 px-3 text-slate-700 leading-tight focus:outline-none focus-visible:border-secondary-500 "
                onChange={event => setProvinceQuery(event.target.value)}
                displayValue={(province: BasicModel) => province.name}
              />
              <Combobox.Button className="absolute inset-y-0 right-0 mt-6 flex border-none items-center pr-2">
                <ChevronUpDownIcon
                  className="h-4 w-4 text-gray-400"
                  aria-hidden="true"
                />
              </Combobox.Button>
              <Combobox.Options className="absolute bg-white shadow-md z-40 mt-1 max-h-32 md:max-h-64 w-full overflow-y-auto rounded ">
                {filteredProvince.map(province => (
                  <Combobox.Option
                    className={({ active }) =>
                      `relative cursor-default select-none text-xs py-2 pl-5 pr-4 ${
                        active ? 'bg-primary-500 text-white' : 'text-gray-900'
                      }`
                    }
                    key={province.name}
                    value={province.name}
                  >
                    {province.name}
                  </Combobox.Option>
                ))}
              </Combobox.Options>
            </Combobox>
            {errors.province && (
              <p className="text-red-500 text-xs italic">
                Province is required
              </p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">City</label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.city
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              }`}
              id="city"
              name="city"
              type="text"
              {...register('city', { required: false })}
            />
            {errors.city && (
              <p className="text-red-500 text-xs italic">City is required</p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Barangay
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.barangay
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              }`}
              id="barangay"
              name="barangay"
              type="text"
              {...register('barangay', { required: false })}
            />
            {errors.barangay && (
              <p className="text-red-500 text-xs italic">
                Barangay is required
              </p>
            )}
          </div>

          <div className="col-span-4 md:col-span-2 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Postal
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.postal
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              }`}
              id="postal"
              name="postal"
              type="number"
              {...register('postal', { required: false })}
            />
            {errors.postal && (
              <p className="text-red-500 text-xs italic">Postal is required</p>
            )}
          </div>
          <div className="col-span-4 mb-2">
            <label className="block text-slate-700 font-bold mb-2">
              Address
            </label>
            <input
              className={`appearance-none border rounded-md  w-full py-2 px-3 leading-tight border-slate-200 bg-slate-50 text-slate-700 ${
                errors.address
                  ? 'border border-red-500 focus:outline focus:outline-red-500'
                  : 'focus:outline focus:outline-secondary-500'
              } `}
              id="address"
              name="address"
              type="text"
              {...register('address', { required: false })}
            />
            {errors.address && (
              <p className="text-red-500 text-xs italic">Address is required</p>
            )}
          </div>
        </Card>
      </LoadingOverlay>
    </form>
  );
}
