/**
 * Asynchronously loads the component for MiscellaneousPage
 */

import { lazyLoad } from 'utils/loadable';

export const MiscellaneousPage = lazyLoad(
  () => import('./index'),
  module => module.MiscellaneousPage,
);
