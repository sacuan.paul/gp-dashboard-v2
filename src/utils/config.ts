import axios from 'axios';
import { Auth } from './auth';
import { apiUrl } from './constants';

const apiClient = axios.create({
  baseURL: `${apiUrl}/v2`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${Auth.token()}`,
  },
});

export const config = {
  apiClient,
};
